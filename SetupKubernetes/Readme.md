# Cài đặt K8s


#### Yêu cầu

1. Cần tối thiểu 3 server. 1 Master & 2 Worker.
2. Cần quyền root để thiết lập.
3. Mở thông port giữ các server Master & Woker.



| Tên máy/Hostname | Thông tin hệ thống                                        | Vai trò             |
| ---------------- | --------------------------------------------------------- | ------------------- |
| master           | HĐH CentOS7, Docker CE, Kubernetes. Địa chỉ IP 10.0.5.220 | Khởi tạo là master  |
| worker1          | HĐH CentOS7, Docker CE, Kubernetes. Địa chỉ IP 10.0.5.221 | Khởi tạo là worker1 |
| worker2          | HĐH CentOS7, Docker CE, Kubernetes. Địa chỉ IP 10.0.5.222 | Khởi tạo là worker2 |



## 1. Cài đặt Master Server

1. Copy file **common.sh** và **InstallKubernetes.sh** vào server.
   
![alt text](./../Images/1.png)


2. Chạy file common.sh với lệnh ```sudo sh common.sh```

![alt text](./../Images/2.png)


3. Chạy lệnh sau để đổi hostname: ```hostnamectl set-hostname master```
4. Cập nhật file hosts với ip master & worker đã định ra sẵn.
```
cat <<EOF>> /etc/hosts
$MASTER_IP $MASTER_NAME
$WORKER_1_IP $WORKER_1_NAME worker-$WORKER_1_NAME
$WORKER_2_IP $WORKER_2_NAME worker-$WORKER_2_NAME
EOF
```

ví dụ:

```
cat <<EOF>> /etc/hosts
10.0.5.220 master
10.0.5.221 worker1
10.0.5.222 worker2
EOF
```

![alt text](./../Images/4.png)


5. Khởi động lại server.
6. Chạy file **InstallKubernetes.sh** với lệnh ```sudo sh InstallKubernetes.sh```

![alt text](./../Images/3.png)

7. Chạy lệnh: ```swapoff -a```
8. Khởi tạo Master, Với địa chỉ IP của master node.
```
kubeadm init --apiserver-advertise-address=10.0.5.220 --pod-network-cidr=192.168.0.0/16
```


![alt text](./../Images/5.png)

9. Copy lại đoạn trên lưu trữ lại.

![alt text](./../Images/6.png)

10. Chạy lệnh: 
```
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```
11. Cài đặt plugin network
```
kubectl apply -f https://docs.projectcalico.org/v3.10/manifests/calico.yaml
```

![alt text](./../Images/7.png)



## 2. Cài đặt Worker


1. Copy file **common.sh** và **InstallKubernetes.sh** vào server.   
2. Chạy file common.sh với lệnh ```sudo sh common.sh```
3. Chạy lệnh sau để đổi hostname: ```hostnamectl set-hostname worker1```

![alt text](./../Images/8.png)

4. Cập nhật file hosts với ip master & worker đã định ra sẵn.
```
cat <<EOF>> /etc/hosts
$MASTER_IP $MASTER_NAME
$WORKER_1_IP $WORKER_1_NAME worker-$WORKER_1_NAME
$WORKER_2_IP $WORKER_2_NAME worker-$WORKER_2_NAME
EOF
```

ví dụ:

```
cat <<EOF>> /etc/hosts
10.0.5.220 master
10.0.5.221 worker1
10.0.5.222 worker2
EOF
```

5. Khởi động lại server.
6. Chạy file **InstallKubernetes.sh** với lệnh ```sudo sh InstallKubernetes.sh```
7. Chạy lệnh: ```swapoff -a```
8. Kết nối Node vào Cluster (Lấy command ở bước 9 ở Master):

```
kubeadm join 10.0.5.220:6443 --token o2a34y.j6h141w0qg4i2d4w --discovery-token-ca-cert-hash sha256:8b4b9f4d85c6d9b96fe7e32f5af0a554e2d09b00bac5a169eb2646347f6fe9b9 
```

![alt text](./../Images/9.png)
9. Làm tương tự với worker 2.
10. Quay lại Master kiểm tra các node có trong Cluster (Chờ 2-3p để Worker ready)

![alt text](./../Images/11.png)
![alt text](./../Images/12.png)