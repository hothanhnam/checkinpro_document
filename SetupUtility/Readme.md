# Cài đặt Rabbit MQ 


## Yêu cầu

1. **rabbitmq:3.9-management**
2. Docker - [Link](https://docs.docker.com/engine/install/rhel/)
3. Docker Compose - [Link](https://docs.docker.com/compose/install/)


## Cài đặt

1. Copy file docker-compose.yml vào server.
2. Gõ lệnh sau để khởi tạo & chạy Rabbit MQ:
```
docker run -d -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3.9-management
```
3. Dùng lệnh ```docker ps``` để kiểm tra status của queue.
4. Truy cập vào Rabbit MQ

   + Username: guest
   + Password: guest

![alt text](./../Images/31.png)

5. Tạo Queue như hình:

   + Name: **CIP_EMAIL_SENDER**

![alt text](./../Images/30.png)

6. Tạo Exchange
   + Name: **EXCHANGE.CIP_EMAIL_SENDER**

![alt text](./../Images/32.png)

7. Click vào EXCHANGE.CIP_EMAIL_SENDER -> để vào chỉnh sửa

![alt text](./../Images/33.png)

8. Binding Exchange với Queue => Bind

![alt text](./../Images/34.png)

9. Kiểm tra bằng cách public message

![alt text](./../Images/35.png)

10. Done