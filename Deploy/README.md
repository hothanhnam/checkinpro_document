# Cài đặt CheckinPro lên K8s


## Yêu Cầu

1. Kubernetes cluster.
2. Quyền root. 
3. Được quyền truy cập vào địa chỉ **registry.unit.vn**


## Cài đặt

1. SSH vào **Master Node**
2. Tạo mới **Namespace** cho CheckinPro

   ```
   kubectl create ns hdbank-checkinpro
   ```



3. Kiểm tra lại namespace vừa tạo.

    ```
    kubectl get ns
    ```

![alt text](./../Images/13.png)

4. Copy tất cả file yml vào Master node
   
![alt text](./../Images/14.png)

5. Tạo Serect key.
```
kubectl create secret docker-registry unit-registry --docker-server=registry.unit.vn --docker-username=cip --docker-password=P@ssw0rd -o yaml > docker-serect.yaml
```

6. Chạy lệnh apply serect yaml

```
kubectl apply -f docker-serect.yaml
```

![alt text](./../Images/18.png)


7. Chạy lần lượt file yml theo thứ tự tăng dần
   
![alt text](./../Images/16.png)
