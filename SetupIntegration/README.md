---
title: CheckinPro Documents
author: Ho Thanh Nam
date: 15/11/2021
---

# Cài đặt Integration Server

### Yêu cầu

1. Cài đặt Internet Information Services 10 (IIS).
2. Cài đặt phần mềm sau (có đính kèm theo hướng dẫn này):
   1. dotnet-sdk-5
   2. dotnet-hosting
   3. mongodb-windows
3. CheckinPro Integration Source.


### Cài đặt

1. Tạo Thư mục & giải nén Source vào thư mục này.

![alt text](./../Images/36.png)

2. Cài đặt sdk & mongoDB xong có thể test lại bằng cách tải MongoDb Compass

![alt text](./../Images/37.png)

3. Mở IIS & Tạo 1 Site mới và thiết lập như hình.

![alt text](./../Images/38.png)

![alt text](./../Images/39.png)

4. Mở trinh duyệt web: **http://localhost:5000/swagger/index.html**

![alt text](./../Images/40.png)

