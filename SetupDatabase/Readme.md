---
title: CheckinPro Documents
author: Ho Thanh Nam
date: 15/11/2021
---

# Cài đặt Database


## Yêu cầu


1. Docker images: **postgres:13.4**, **dpage/pgadmin4**
2. Docker - [Link](https://docs.docker.com/engine/install/rhel/)
3. Docker Compose - [Link](https://docs.docker.com/compose/install/)


## Cài đặt

1. Copy file docker-compose.yml vào server.
2. Gõ lệnh sau để khởi tạo & chạy database:
```
docker-compose up -d
```
3. Dùng lệnh ```docker ps``` để kiểm tra status của database.

![alt text](./../Images/19.png)

4. Mở trình duyệt web truy cập vào IP:8000 (IP: Ip của server)

![alt text](./../Images/20.png)


5. Nhập password : **P@ssw0rd** , Password này được gán ở dòng 20 trong docker-compose.yml
6. Tạo mới Server

![alt text](./../Images/21.png)

7. Thông tin đăng nhập:

     + Name: **CheckinPro**
     + Port: **5432** [được gán ở dòng 10 trong docker-compose.yml]
     + Username: **postgres**
     + Password" **P@ssw0rd**, Password này được gán ở dòng 8 trong docker-compose.yml

![alt text](./../Images/23.png)

8. Tạo database CheckinPro với định dạng UTF-8

![alt text](./../Images/24.png)

![alt text](./../Images/25.png)

9. Copy file hdbank_{version}.sql vào server.
10. Lấy Container Id của pgAdmin từ bước 3. Dùng lệnh docker cp để copy file vào container
    
```
docker cp foo.txt container_id:/foo.txt
```

![alt text](./../Images/27.png)

11. Restore database từ file hdbank_{version}.sql


![alt text](./../Images/26.png)

12. Chọn file & restore

![alt text](./../Images/28.png)

13. Done.

![alt text](./../Images/29.png)